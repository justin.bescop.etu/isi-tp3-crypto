#!/bin/bash
rm -rf disk usb1 usb2 ramDisk
mkdir usb1 usb2 disk ramDisk
touch disk/listeCartes.txt

dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usb1/pass1.txt
dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usb2/pass2.txt
openssl enc -aes-256-cbc -in usb1/pass1.txt -out usb1/pass1.cbc
openssl enc -aes-256-cbc -in usb2/pass2.txt -out usb2/pass2.cbc

dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 "%02x"' > masterkey.txt
openssl enc -aes-256-ecb -in disk/listeCartes.txt -out disk/listeCartes.cbc -K $(cat masterkey.txt)
cat masterkey.txt
openssl enc -aes-256-ecb -in masterkey.txt -out masterkey1.cbc -K $(cat usb1/pass1.txt)
openssl enc -aes-256-ecb -in masterkey1.cbc -out disk/masterkey.cbc -K $(cat usb2/pass2.txt)

rm usb1/pass1.txt usb2/pass2.txt masterkey.txt masterkey1.cbc disk/listeCartes.txt




