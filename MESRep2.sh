#!/bin/bash 
openssl enc -d -aes-256-cbc -in usb1/pass1.cbc -out ramDisk/pass1.txt 
openssl enc -d -aes-256-cbc -in usbRep2/passRep2.cbc -out ramDisk/passRep2.txt 
openssl enc -d -aes-256-ecb -in disk/masterkey.cbc -out ramDisk/desencStep1.cbc -K $(cat ramDisk/passRep2.txt) 
openssl enc -d -aes-256-ecb -in ramDisk/desencStep1.cbc -out ramDisk/desenc.txt -K $(cat ramDisk/pass1.txt) 

cat ramDisk/desenc.txt 

rm ramDisk/pass1.txt usbRep2/passRep2.txt ramDisk/desencStep1.cbc

