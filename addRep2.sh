#!/bin/bash
mkdir usbRep2

dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usbRep2/passRep2.txt
openssl enc -aes-256-cbc -in usbRep2/passRep2.txt -out usbRep1/passRep2.cbc

openssl enc -d -aes-256-cbc -in usb1/pass1.cbc -out ramDisk/pass1.txt 

openssl enc -aes-256-ecb -in ramDisk/desenc.txt -out ramDisk/masterkey1.cbc -K $(cat ramDisk/pass1.txt)
openssl enc -aes-256-ecb -in ramDisk/masterkey1.cbc -out disk/masterkeyRep2.cbc -K $(cat usbRep2/passRep2.txt)

rm ramDisk/pass1.txt ramDisk/masterkey1.cbc