#!/bin/bash
mkdir usbRep1

dd if=/dev/urandom bs=32 count=1 | hexdump -ve '1/1 "%02x"' > usbRep1/passRep1.txt
openssl enc -aes-256-cbc -in usbRep1/passRep1.txt -out usbRep1/passRep1.cbc

openssl enc -d -aes-256-cbc -in usb1/pass2.cbc -out ramDisk/pass2.txt

openssl enc -aes-256-ecb -in ramDisk/desenc.txt -out ramDisk/masterkey1.cbc -K $(cat usbRep1/passRep1.txt)
openssl enc -aes-256-ecb -in ramDisk/masterkey1.cbc -out disk/masterkeyRep1.cbc -K $(cat ramDisk/pass2.txt)

rm ramDisk/pass2.txt ramDisk/masterkey1.cbc usbRep1/passRep1.txt
