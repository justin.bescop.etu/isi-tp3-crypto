#!/bin/bash

openssl enc -d -aes-256-ecb -in disk/listeCartes.cbc -out ramDisk/listeCartes.txt -K $(cat ramDisk/desenc.txt)

python3 findLine.py $1

openssl enc -aes-256-ecb -in ramDisk/listeCartes.txt -out disk/listeCartes.cbc -K $(cat ramDisk/desenc.txt)

rm ramDisk/listeCartes.txt
