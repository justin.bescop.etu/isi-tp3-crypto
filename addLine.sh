#!/bin/bash

openssl enc -d -aes-256-ecb -in disk/listeCartes.cbc -out ramDisk/listeCartes.txt -K $(cat ramDisk/desenc.txt)

python3 addLine.py $1 $2

openssl enc -aes-256-ecb -in ramDisk/listeCartes.txt -out disk/listeCartes.cbc -K $(cat ramDisk/desenc.txt)

rm ramDisk/listeCartes.txt
