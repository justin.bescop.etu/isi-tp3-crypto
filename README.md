# Rendu "Crypto"

## Binome

* Bescop, Justin, email: justin.bescop.etu@univ-lille.fr

* Ferdinand, Mathieu, email: mathieu.ferdinand.etu@univ-lille.fr


## Question 1

* Si on crypte le fichier du serveur 2 fois avec comme clé les fichiers sur les 2 clés usb des 2 responsables puis qu'on demande les 2 mots de passe au lancement du serveur,alors le serveur est protégé par une double authentification a 2 facteurs 

* Ce qu'on a compris :
Lors du démarrage du serveur, une clé est générée et stockée dans un RAM disk.Cette clé va chiffrer le fichier contenant les cartes. 2 clés usb doivent être insérée lors du démarrage

Il faudra une commande initialisation qui va initialiser les 2 mdp, les 2 clés usb et le ram disque avec sa clé de cryptage. Dans les clés usb on va chiffrer un fichier avec la clé générée dans le ram disk. 

La commande mise en service va tout vérifier en déchiffrant les infos avec la clé dans le ram disk.

Probleme : quand on éteint le serveur on perd la clé et du coup on pourra plus déchiffrer les fichiers.

Initialisation : Dans cette étape on va créer les clés aléatoires dans les 2 clés usb (sous forme de fichier) qui seront elles même cryptées par 2 mots de passe (1 chacun) . En effet, avec openssl on peut crypter un fichier avec un mot de passe (le mot de passe haché sera la clé de cryptage). 
On va aussi créer le fichier qui recevra nos données et qui sera crypté.
On va aussi pour la démonstration créer par la même occasion les dossiers qui correspondront à nos clés et le dossier RAMdisk.
Notre modèle va s'appuyer sur une clé aléatoire cryptée stockée dans le disque du serveur qui sera ensuite décryptée et mise dans la ram lors de la mise en service.
Dans l'initialisation on va donc générer une clé aléatoire qui sera notre clé de déchiffrage du fichier "sensible". Pour des raisons de sécurité on ne peut pas la stocker telle quelle dans le disque. On va donc faire un double chiffrement de cette clé grâce aux deux clés stockée sur nos usb. C'est cette clé doublement chiffrée que l'on s'autorisera à stocker dans le serveur.

Mise en service : Le but de la mise en service va être de stocker la clé de dechiffrement en clair dans la RAM pour pouvoir facilement effectuer des opérations sur notre fichier. A ce moment la donc on va demander à nouveau les usb pour récuperer les clés qui auront été déchiffrer avec le mot de passe. Puis avec ces clé on va déchiffrer la "master key" stockée sur le disque et la mettre dans le RAMdisk.
On se permet de stocker la clé en clair dans le RAMdisk car c'est une mémoire qui sera effacé si le serveur venait à être volé.

Délégation de droit : On demande maintenant de permettre une remise en service si l'un des deux admin est absent. Pour ce faire on va désigner 2 représentants légaux. On choisi donc pour notre modèle que la clé aléatoire sera stocké 3 fois sur notre disque avec des combinaisons de chiffrement différents. 1 chiffrement en utilisant les clés des 2 admins si ils sont disponibles. Un deuxieme chiffrement avec la clé du représentant d'un admin et la clé de l'autre admin.
Et un dernier pour le cas oou l'autre admin est absent. Ansi on aura toutes les configs possibles.

## Question 2

* initialisation: init.sh
* mise en service : MES.sh
* Effacer une ligne : delLine.sh
* Ajouter une ligne : addLine.sh
* Trouver une ligne : findLine.sh

* Pour les différents services comme ajouter une ligne, supprimer une ligne ou trouver une ligne on a choisi d'utiliser une combinaison de commande bash + code python.
Pour ce qui est de la partie bash elle s'occupe simplement de décrypter le fichier sensible "listeCartes.cbc" et de le stocker en clair prêt à modification ou lecture dans le ramDisk. Après execution du code python le bash va remplacer le fichier crypté dans le disk et nettoyer les fichiers temporaires.
Pour ce qui est du code python on fait des actions simples: lecture/ecriture.
Pour relier les 2 codes on utilise les paramètres bash et python.

* Guide d'utilisation des fonctions :
* addLine.sh : Demande 2 parametres -> la paire a insérer (nom + numéro de carte)
* delLine.sh : Demande 2 parametres -> la paire à supprimer
* findLine.sh : Demande 1 parametres -> Le nom pour lequel on veut les numéros de carte.

## Question 3
* Pour adapter notre modele pour ajouter des représentants aux admins ils suffirait alors de rajouter un fichier dans le serveur qui correspondrait à la masterkey cryptée par un admin et un représentant. On aurait alors 2 fichiers qui représentent la masterkey : un qui est crypté par les 2 admins et un par un admin et le représentant du deuxieme, on peut alors récupérer la masterkey avec un admin et un représentant mais on fait toujours la différence avec la récuperation de la masterkey par les 2 admins

## Question 4
* Pour s'adapter à ce changement, le service de démarrage n'a pas besoin d'etre changé mais il faut rajouter une commande pour ajouter un représentant (qui rajoute simplement un encrypté de la masterkey) et executer la mise en service avec les représentants. 
* Dans notre cas on a rajouté addRep1.sh et addRep2.sh qui permettent d'ajouter des représentants respectivement à l'admin 1 ou 2 et MESRep1.sh, MESRep2.sh qui permettent d'effectuer la mise en service avec des représentants plutôt que les admins
* MESRep1.sh et MESRep2.sh permettent alors d'obtenir la masterkey habituelle qui permet d'effectuer les autres actions de façon normale 

Fonction utiles :
shasum -a 256 --> hashe le fichier ou texte passé via l'entrée standard et ressort un hash sutr 256 bits.

dd if=/dev/random bs=32 count=1 | hexdump -ve '1/1 "%02x"' > pass.txt ça créé les random pour les clés usb
openssl enc -aes-256-ecb -nopad -in (la valeur random pour la cle usb) -out (dans la cle usb) -K $(cat pass.txt) on change la fin pour utiliser le mot de passe du mec
openssl enc -d -aes-256-ecb -nopad -in (le fichier cripté de la cle usb) -out dec.txt -K $(cat pass.txt)


